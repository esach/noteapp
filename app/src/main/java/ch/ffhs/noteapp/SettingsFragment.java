package ch.ffhs.noteapp;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by julian on 11.12.2014.
 * Date: 11.12.2014
 */
public class SettingsFragment extends Fragment implements View.OnClickListener, Animation.AnimationListener {
    private View katview;
    private View persview;
    private Button kategorie;
    private Button personal;
    private Button speichern;
    private Button button;
    private String PersID;
    private ContentValues values;
    private Animation animPushOut;
    private NotesDatabase db;
    private SQLiteDatabase database;
    private UserSettings user;
    private ArrayAdapter<String> adapter;
    private ListView listKat;

    public SettingsFragment() {
        listKat = null;
        adapter = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.settingsfragment,
                container, false);
        db = new NotesDatabase(this.getActivity());
        database = db.getReadableDatabase();
        user = new UserSettings(database);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kategorie = (Button) view.findViewById(R.id.katBtn);
        kategorie.setOnClickListener(this);
        personal = (Button) view.findViewById(R.id.persBtn);
        personal.setOnClickListener(this);
        speichern = (Button) view.findViewById(R.id.btnPersSave);
        speichern.setOnClickListener(this);
        button = (Button) view.findViewById(R.id.btnNewCategory);
        button.setOnClickListener(this);
        katview = view.findViewById(R.id.katid);
        katview.setVisibility(View.GONE);
        persview = view.findViewById(R.id.persid);
        persview.setVisibility(View.GONE);
        listKat = (ListView) view.findViewById(R.id.lstKategories);
        int res = android.R.layout.simple_list_item_1;
        adapter = new ArrayAdapter<String>(this.getActivity(), res
                , MainActivity.KategorieList);
        listKat.setAdapter(adapter);

        animPushOut = AnimationUtils.loadAnimation(this.getActivity(), R.anim.push_out_left);
        animPushOut.setAnimationListener(this);

        listKat.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                final String listItem = MainActivity.KategorieList.get(position).toString();
                if (!listItem.endsWith("Cat_1")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Löschen");
                    alertDialog.setMessage("Wollen sie die Kategorie " + listItem + " löschen?");
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            view.animate().setDuration(300).translationX(parent.getWidth() + 100)
                                    .withEndAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            MainActivity.KategorieList.remove(item);
                                            adapter.notifyDataSetChanged();
                                            view.setX(0);
                                            db.delCategory(database, listItem);
                                        }
                                    });

                        }
                    });
                    alertDialog.setButton2("Cancle", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();

                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Löschen");
                    alertDialog.setMessage("Diese Kategorie " + listItem + " kan nicht gelöscht werden?");
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
        });

        getView().findViewById(R.id.settingsview).invalidate();
        PersID = user.getvPersID();
        TextView txtname = (TextView) view.findViewById(R.id.txtname);
        txtname.setText(user.getvSurename());
        TextView txtPrename = (TextView) view.findViewById(R.id.txtPrename);
        txtPrename.setText(user.getvPrename());
        TextView txtStreet = (TextView) view.findViewById(R.id.txtStreet);
        txtStreet.setText(user.getvStreet());
        TextView txtPLZ = (TextView) view.findViewById(R.id.txtPLZ);
        txtPLZ.setText(user.getvPLZ());
        TextView txtPlace = (TextView) view.findViewById(R.id.txtPlace);
        txtPlace.setText(user.getvPlace());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.katBtn:
                katview.setVisibility(katview.isShown()
                        ? View.GONE
                        : View.VISIBLE);
                break;
            case R.id.persBtn:
                persview.setVisibility(persview.isShown()
                        ? View.GONE
                        : View.VISIBLE);
                break;
            case R.id.btnPersSave:
                EditText editname = (EditText) getActivity().findViewById(R.id.txtname);
                EditText editPrename = (EditText) getActivity().findViewById(R.id.txtPrename);
                EditText editStreet = (EditText) getActivity().findViewById(R.id.txtStreet);
                EditText editPLZ = (EditText) getActivity().findViewById(R.id.txtPLZ);
                EditText editPlace = (EditText) getActivity().findViewById(R.id.txtPlace);
                user.setUser(db, database, PersID, editPrename.getText().toString(), editname.getText().toString(), editPLZ.getText().toString(), editPlace.getText().toString(), editStreet.getText().toString());
                break;
            case R.id.btnNewCategory:
                EditText newCategory = (EditText) getActivity().findViewById(R.id.newCategory);
                if (MainActivity.KategorieList.contains(newCategory.getText().toString())) {
                    AlertDialog alertDialog1 = new AlertDialog.Builder(this.getActivity()).create();
                    alertDialog1.setTitle("Kategorie");
                    alertDialog1.setMessage("Die Kategorie " + newCategory.getText().toString() + " gibt es schon!");
                    alertDialog1.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog1.show();
                } else {
                    MainActivity.KategorieList.add(newCategory.getText().toString());
                    values = new ContentValues();
                    values.put("Name", newCategory.getText().toString());
                    db.newCategory(database, values);
                    TextView txtCategory = (TextView) getActivity().findViewById(R.id.newCategory);
                    txtCategory.setText("");
                    int res = android.R.layout.simple_list_item_1;
                    adapter.notifyDataSetChanged();

                }
                break;
        }
        getView().findViewById(R.id.settingsview).invalidate();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
