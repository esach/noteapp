package ch.ffhs.noteapp;

/**
 * Created by julian on 03.12.2014.
 * Date: 03.12.2014
 */
public class Kategorie {
    private int vID;

    /**
     * @param vName Name
     * @param ID    id
     * @author Julian Frey
     */
    public Kategorie(String vName, int ID) {
        this.vID = ID;
    }

    /**
     * Getter für die Kategorie ID
     *
     * @return ID von der Kategorie
     * @author Julian Frey
     */
    public int getvID() {
        return vID;
    }
}
