package ch.ffhs.noteapp.expandableListView.Classes;

/**
 * Created by julian on 09.12.2014.
 */
public class ExpandableListChild {

    private String Name;
    private String Tag;

    public String getName() {
        return Name;
    }
    public void setName(String Name) {
        this.Name = Name;
    }
    public String getTag() {
        return Tag;
    }
    public void setTag(String Tag) {
        this.Tag = Tag;
    }
}

