package ch.ffhs.noteapp.expandableListView.Classes;

/**
 * Created by julian on 09.12.2014.
 */
import java.util.ArrayList;

import ch.ffhs.noteapp.expandableListView.Classes.ExpandableListChild;

public class ExpandableListGroup {

    private String Name;
    private ArrayList<ExpandableListChild> Items;

    public String getName() {
        return Name;
    }
    public void setName(String name) {
        this.Name = name;
    }
    public ArrayList<ExpandableListChild> getItems() {
        return Items;
    }
    public void setItems(ArrayList<ExpandableListChild> Items) {
        this.Items = Items;
    }


}

