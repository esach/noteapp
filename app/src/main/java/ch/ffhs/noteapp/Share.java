package ch.ffhs.noteapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Editable;
import android.util.Base64;
import android.util.Log;
import android.util.Xml;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by Julian Frey on 03.12.2014.
 * Date: 03.12.2014
 */
public class Share extends Activity {
    private String vName;
    private String vEmail;
    private Context context;
    private File f;

    /**
     * @param vID ID
     * @author Julian Frey
     */
    public Share(String vID) {
    }

    /**
     * @param fullNote, context
     * @author Julian Frey
     */
    public Share(Context context, Notiz fullNote) {
        this.context = context;

        try {
            createXML(fullNote, fullNote.getvName());
        } catch (SAXException e) {
            e.printStackTrace();
        }

    }

    public Share() {

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    /**
     * Getter für den User Name
     *
     * @return Name des Users
     * @author Julian Frey
     */


    public String getName() {
        return this.vName;
    }

    /**
     * Setter für den User Name
     *
     * @param name Name
     * @author Julian Frey
     */
    public void setName(String name) {
        this.vName = name;
    }

    /**
     * Getter für den User Name
     *
     * @return Name des Users
     * @author Julian Frey
     */
    public String getvName() {
        return vName;
    }

    /**
     * Getter für die Email Adresse
     *
     * @return Email Adresse
     * @author Julian Frey
     */
    public String getEmail() {
        return this.vEmail;
    }

    /**
     * Setter für die Email Adresse
     *
     * @param email email
     * @author Julian Frey
     */
    public void setEmail(String email) {
        this.vEmail = email;
    }

    public void createXML(final Notiz note, String notename) throws SAXException {
        f = new File(context.getCacheDir(), notename + ".ffhs");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream myFile = new FileOutputStream(f);


            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();

            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag("", "Notiz");
            xmlSerializer.attribute("", "Name", note.getvName());
            xmlSerializer.attribute("", "Ort", note.getvPlace());
            xmlSerializer.attribute("", "Datum", note.getvDate());
            xmlSerializer.attribute("", "Keywords", note.getaKeyword());
            xmlSerializer.attribute("", "Note", note.getvDescription());
            xmlSerializer.attribute("", "Category", note.getoKategorie());
            xmlSerializer.startTag("", "Pictures");
            for (int i = 0; i < note.gethPicture().size(); i++) {
                Uri uri = Uri.parse(note.gethPicture().get(i));
                Bitmap bm = BitmapFactory.decodeFile(uri.getPath());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                xmlSerializer.attribute("", "Bild", encodedImage);
            }
            xmlSerializer.endTag("", "Pictures");
            xmlSerializer.startTag("", "Sounds");
            for (int i = 0; i < note.gethSound().size(); i++) {
                File file = new File(note.gethSound().get(i));
                byte[] bytes = FileUtils.readFileToByteArray(file);
                String encoded = Base64.encodeToString(bytes, 0);
                xmlSerializer.attribute("", "Sound", encoded);
            }
            xmlSerializer.endTag("", "Sounds");
            xmlSerializer.endTag("", "Notiz");
            xmlSerializer.endDocument();

            writer.toString();
            myFile.write(writer.toString().getBytes());
            myFile.flush();
            myFile.close();
            f.setReadable(true);

            final EditText input = new EditText(context);

            new AlertDialog.Builder(context)
                    .setTitle("Empfaenger hinzufuegen")
                    .setMessage("Empfaenger")
                    .setView(input)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Editable value = input.getText();
                            sendMail(value.toString());
                            note.addShare(value.toString());

                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).show();


        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
            throw new SAXException(e);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendMail(String mail) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{mail});
        i.putExtra(Intent.EXTRA_SUBJECT, "Notiz");
        i.putExtra(Intent.EXTRA_TEXT, "Notiz");
        Uri uri = Uri.parse("content://" + CachedFileProvider.AUTHORITY + "/" + f.getName());
        Log.d("PATH", uri.getEncodedPath());
        i.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri.toString()));
        try {
            context.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
