package ch.ffhs.noteapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Samuel on 11.12.2014.
 * Date: 11.12.2014
 */
public class NotesDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Notes.db";
    private static final int DATABASE_VERSION = 16;
    private static NotesDatabase db;
    private static SQLiteDatabase database;

    /**
     * Erstellen der Datenbank
     *
     * @param context
     * @author Samuel Weidmann
     */
    public NotesDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Update der Infomationen in der Tabelle PersonalData
     *
     * @param db
     * @param values
     * @param PersID
     * @author Samuel Weidmann
     */
    public static void updateUser(SQLiteDatabase db, ContentValues values, String PersID) {
        db.update("PersonalData", values, "id=" + PersID, null);
    }

    /**
     * Einfügen einer neune Kategorie in der Tabelle Category
     *
     * @param db
     * @param values
     * @author Samuel Weidmann
     */
    public static void newCategory(SQLiteDatabase db, ContentValues values) {
        db.insert("Category", null, values);
        MainActivity.createKategorie();
    }

    /**
     * Löschen einer Kategorie in der Tabelle Category
     * <p>
     * Weiter wird noch in der Note Tablle bei allen Notizen die die zu löschende Kategorie haben die Default Kategorie eingetragen.
     * </p>
     *
     * @param db       datenbank
     * @param Category Kategorie
     * @author Samuel Weidmann
     */
    public static void delCategory(SQLiteDatabase db, String Category) {
        String Cat_ID = null;
        String selectQuery = "SELECT * FROM Category where Name='" + Category + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            Cat_ID = cursor.getString(0);
        }
        ContentValues values = null;
        values = new ContentValues();
        values.put("Category_Id", "1");
        db.update("Note", values, "Category_id=" + Cat_ID, null);
        db.delete("Category", "ID=" + Cat_ID, null);
    }

    /**
     * Erstellen aller Tabellen der Datenbank
     *
     * @param db datenbank
     * @author Samuel Weidmann
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS " + "Category" + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + "PersonalData" + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, Surname TEXT, Firstname TEXT, Street TEXT, City TEXT, Plz INTEGER);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + "Picture" + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, Picture TEXT, Note_Id INTEGER);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + "Sound" + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, Sound TEXT, Note_Id INTEGER);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + "Share" + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, Email TEXT, Note_Id INTEGER);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + "Note" + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT, Keywords TEXT, Description TEXT, City TEXT, Category_Id INTEGER, Date DATETIME);");
        fillDB(db);
    }

    /**
     * Update der Datenbank (löschen und neu erstellen der Tabellen)
     *
     * @param db         datenbank
     * @param oldVersion alte version
     * @param newVersion neue Version
     * @author Samuel Weidmann
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + "Category");
        db.execSQL("DROP TABLE IF EXISTS " + "PersonalData");
        db.execSQL("DROP TABLE IF EXISTS " + "Picture");
        db.execSQL("DROP TABLE IF EXISTS " + "Sound");
        db.execSQL("DROP TABLE IF EXISTS " + "Share");
        db.execSQL("DROP TABLE IF EXISTS " + "Note");
        onCreate(db);
    }

    /**
     * Beispiel Daten in die Datenbank einspielen
     *
     * @param db datenbank
     * @author Samuel Weidmann
     */
    private void fillDB(SQLiteDatabase db) {

        ContentValues values = null;
        values = new ContentValues();
        values.put("Name", "Cat_1");
        db.insert("Category", null, values);

        values = new ContentValues();
        values.put("Name", "Cat_2");
        db.insert("Category", null, values);

        values = new ContentValues();
        values.put("Name", "Cat_3");
        db.insert("Category", null, values);

        values = new ContentValues();
        values.put("Name", "Cat_4");
        db.insert("Category", null, values);

        values = new ContentValues();
        values.put("Surname", "Weidmann");
        values.put("Firstname", "Samuel");
        values.put("Street", "Winkelackerstr 26");
        values.put("City", "Dulliken");
        values.put("Plz", "4657");
        db.insert("PersonalData", null, values);

        values = new ContentValues();
        values.put("Title", "Notiz 1");
        values.put("Keywords", "Samuel, Hugo, usw");
        values.put("Description", "Dies ist eine Notiz1");
        values.put("City", "Dulliken");
        values.put("Category_Id", "1");
        values.put("Date", "2014.12.12");
        db.insert("Note", null, values);

        values = new ContentValues();
        values.put("Title", "Notiz 2");
        values.put("Keywords", "Samuel, Hugo, usw");
        values.put("Description", "Dies ist eine Notiz2");
        values.put("City", "Gösgen");
        values.put("Category_Id", "2");
        values.put("Date", "2014.12.12");
        db.insert("Note", null, values);

        values = new ContentValues();
        values.put("Title", "Notiz 3");
        values.put("Keywords", "Samuel, Hugo, usw");
        values.put("Description", "Dies ist eine Notiz3");
        values.put("City", "Olten");
        values.put("Category_Id", "1");
        values.put("Date", "2014.12.12");
        db.insert("Note", null, values);

        values = new ContentValues();
        values.put("Title", "Notiz 4");
        values.put("Keywords", "Samuel, Hugo, usw");
        values.put("Description", "Dies ist eine Notiz4");
        values.put("City", "Trimbach");
        values.put("Category_Id", "4");
        values.put("Date", "2014.12.12");
        db.insert("Note", null, values);
    }

}
