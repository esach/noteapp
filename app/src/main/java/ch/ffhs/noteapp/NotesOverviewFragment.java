package ch.ffhs.noteapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import ch.ffhs.noteapp.expandableListView.Adapter.ExpandableListAdapter;
import ch.ffhs.noteapp.expandableListView.Classes.ExpandableListChild;
import ch.ffhs.noteapp.expandableListView.Classes.ExpandableListGroup;

/**
 * Created by Julian Frey on 10.12.2014.
 * Date: 10.12.2014
 */
public class NotesOverviewFragment extends Fragment implements ExpandableListView.OnChildClickListener {
    public static List SpinnerList = new ArrayList<String>();
    Map<String, Notiz> hNotiz;
    Spinner spinner;
    private ExpandableListAdapter ExpAdapter;
    private ArrayList<ExpandableListGroup> ExpListItems;
    private ExpandableListView ExpandList;
    private View searchBoxLayout;
    private CheckBox chkBoxplace;
    private CheckBox chkBoxKeywords;
    private CheckBox chkBoxDescription;

    /**
     * @param inflater           inflater
     * @param container          container
     * @param savedInstanceState instance state
     * @return view
     * @author Julian Frey
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notesoverviewfragment,
                container, false);
        super.onCreate(savedInstanceState);
        hNotiz = MainActivity.hNotiz;
        return view;
    }

    /**
     * @param view               view
     * @param savedInstanceState instance state
     * @author Julian Frey
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        SpinnerList.clear();
        SpinnerList.add("Alle");
        SpinnerList.addAll(MainActivity.KategorieList);
        spinner = (Spinner) getView().findViewById(R.id.spinner);
        searchBoxLayout = (LinearLayout) view.findViewById(R.id.searchBoxLayout);
        searchBoxLayout.setVisibility(View.GONE);
        SearchView searchView = (SearchView) view.findViewById(R.id.noteSearch);
        chkBoxDescription = (CheckBox) view.findViewById(R.id.searchBoxDescription);
        chkBoxKeywords = (CheckBox) view.findViewById(R.id.searchBoxKeywords);
        chkBoxplace = (CheckBox) view.findViewById(R.id.searchBoxPlace);
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (searchBoxLayout.isShown()) {
                    searchBoxLayout.setVisibility(View.GONE);
                } else {
                    searchBoxLayout.setVisibility(View.VISIBLE);
                    hNotiz = MainActivity.hNotiz;
                    ExpListItems = SetStandardGroups();
                    ExpAdapter = new ExpandableListAdapter(getActivity(), ExpListItems);
                    ExpandList.setAdapter(ExpAdapter);
                }
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                hNotiz = MainActivity.hNotiz;
                Map<String, Notiz> tmphMap = new HashMap<String, Notiz>();
                Boolean added = Boolean.valueOf(false);
                for (String key : hNotiz.keySet()) {
                    added = false;
                    if (chkBoxplace.isChecked() && added == false) {
                        if (Pattern.compile(Pattern.quote(hNotiz.get(key).getvPlace()), Pattern.CASE_INSENSITIVE).matcher(query).find()) {
                            tmphMap.put(hNotiz.get(key).getvNotizID(), hNotiz.get(key));
                            added = true;
                        }
                    }
                    if (chkBoxKeywords.isChecked() && added == false) {
                        if (Pattern.compile(Pattern.quote(hNotiz.get(key).getaKeyword()), Pattern.CASE_INSENSITIVE).matcher(query).find()) {
                            tmphMap.put(hNotiz.get(key).getvNotizID(), hNotiz.get(key));
                            added = true;
                        }
                    }
                    if (chkBoxDescription.isChecked() && added == false) {
                        if (Pattern.compile(Pattern.quote(hNotiz.get(key).getvDescription()), Pattern.CASE_INSENSITIVE).matcher(query).find()) {
                            tmphMap.put(hNotiz.get(key).getvNotizID(), hNotiz.get(key));
                            added = true;
                        }
                    }
                }
                hNotiz = tmphMap;
                ExpListItems = SetStandardGroups();
                ExpAdapter = new ExpandableListAdapter(getActivity(), ExpListItems);
                ExpandList.setAdapter(ExpAdapter);
                ExpAdapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                hNotiz = MainActivity.hNotiz;
                Map<String, Notiz> tmphMap = new HashMap<String, Notiz>();
                Boolean added = new Boolean(false);
                for (String key : hNotiz.keySet()) {
                    added = false;
                    if (chkBoxplace.isChecked() && added == false) {
                        if (hNotiz.get(key).getvPlace().equals(newText)) {
                            tmphMap.put(hNotiz.get(key).getvNotizID(), hNotiz.get(key));
                            added = true;
                        }
                    }
                    if (chkBoxKeywords.isChecked() && added == false) {
                        if (hNotiz.get(key).getaKeyword().contains(newText)) {
                            tmphMap.put(hNotiz.get(key).getvNotizID(), hNotiz.get(key));
                            added = true;
                        }
                    }
                    if (chkBoxDescription.isChecked() && added == false) {
                        if (hNotiz.get(key).getvDescription().contains(newText)) {
                            tmphMap.put(hNotiz.get(key).getvNotizID(), hNotiz.get(key));
                            added = true;
                        }
                    }
                }
                hNotiz = tmphMap;
                ExpListItems = SetStandardGroups();
                ExpAdapter = new ExpandableListAdapter(getActivity(), ExpListItems);
                ExpandList.setAdapter(ExpAdapter);
                ExpAdapter.notifyDataSetChanged();
                return false;
            }
        });


        ExpandList = (ExpandableListView) getView().findViewById(R.id.expandableListView);
        ExpListItems = SetStandardGroups();
        ExpAdapter = new ExpandableListAdapter(getActivity(), ExpListItems);
        ExpandList.setAdapter(ExpAdapter);
        ExpandList.setOnChildClickListener(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, SpinnerList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ExpListItems = SetStandardGroups();
                ExpAdapter = new ExpandableListAdapter(getActivity(), ExpListItems);
                ExpandList.setAdapter(ExpAdapter);


                ExpAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * @return ArrayList
     * @author Julian Frey
     */
    public ArrayList<ExpandableListGroup> SetStandardGroups() {
        ArrayList<ExpandableListGroup> list = new ArrayList<ExpandableListGroup>();
        ArrayList<ExpandableListChild> list2;
        for (String key : hNotiz.keySet()) {
            if (spinner.getSelectedItem() == null || spinner.getSelectedItem().equals("Alle")) {
                ExpandableListGroup tmpg = new ExpandableListGroup();
                tmpg.setName(hNotiz.get(key).getvName());
                list2 = new ArrayList<ExpandableListChild>();

                ExpandableListChild tmpc0 = new ExpandableListChild();
                tmpc0.setName(hNotiz.get(key).getvName());
                list2.add(tmpc0);
                ExpandableListChild tmpc1 = new ExpandableListChild();
                tmpc1.setName(hNotiz.get(key).getvPlace());
                list2.add(tmpc1);
                ExpandableListChild tmpc2 = new ExpandableListChild();
                tmpc2.setName(hNotiz.get(key).getvDescription());
                list2.add(tmpc2);
                tmpg.setItems(list2);
                list.add(tmpg);
            } else if (spinner.getSelectedItem().equals(hNotiz.get(key).getoKategorie())) {
                ExpandableListGroup tmpg = new ExpandableListGroup();
                tmpg.setName(hNotiz.get(key).getvName());
                list2 = new ArrayList<ExpandableListChild>();

                ExpandableListChild tmpc0 = new ExpandableListChild();
                tmpc0.setName(hNotiz.get(key).getvName());
                list2.add(tmpc0);
                ExpandableListChild tmpc1 = new ExpandableListChild();
                tmpc1.setName(hNotiz.get(key).getvPlace());
                list2.add(tmpc1);
                ExpandableListChild tmpc2 = new ExpandableListChild();
                tmpc2.setName(hNotiz.get(key).getvDescription());
                list2.add(tmpc2);
                tmpg.setItems(list2);
                list.add(tmpg);
            }
        }
        return list;
    }


    /**
     * @param parent        parrent ExpandableList
     * @param v             View
     * @param groupPosition Position
     * @param childPosition Position
     * @param id            id
     * @return boolean
     * @author Julian Frey
     */
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        android.widget.ExpandableListAdapter myadapt = parent.getExpandableListAdapter();
        ExpandableListGroup myGroup = (ExpandableListGroup) myadapt.getGroup(groupPosition);
        String groupName = myGroup.getName();

        FragmentManager fm = getActivity().getFragmentManager();

        CreateNotesFragment currentNote = new CreateNotesFragment();
        for (String key : hNotiz.keySet()) {
            Notiz tmpNote = hNotiz.get(key);
            if (tmpNote.getvName().equals(groupName)) {
                currentNote.setFullNote(tmpNote);
                fm.beginTransaction().replace(R.id.fragment_container, currentNote, "overview").commit();
                FragmentTransaction ft = fm.beginTransaction();
                ft.commit();
            }
        }
        return false;
    }
}
