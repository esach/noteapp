package ch.ffhs.noteapp;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by julian on 11.12.2014.
 * Date: 11.12.2014
 */
public class CreateNotesFragment extends Fragment implements View.OnClickListener {
    static final int REQUEST_TAKE_PHOTO;
    private static final String LOG_TAG = "CreateNotes";


    static Context context;
    private View viewShare;
    private View viewPicture;
    private View viewSound;

    private Button btnStopSound;
    private Button btnStartSound;
    private Notiz fullNote;
    private EditText txttitle;
    private EditText txtkeywords;
    private EditText txtplace;
    private EditText txtdescription;
    private Spinner spinner;
    private MediaRecorder mediaRecorder;


    /**
     *
     *
     * @author Julian Frey
     */
    public CreateNotesFragment() {
    }

    static {
        REQUEST_TAKE_PHOTO = 1;
    }

    /**
     * Erstellen des Thumbnail f&uuml;r das darstellen der Bilder
     *
     * @author Julian Frey
     *
     * @param uri uri for thumbnail
     * @param  THUMBNAIL_SIZE size of Thumbnail
     * @return getThumbnail
     * @throws IOException
     */
    public static Bitmap getThumbnail(Uri uri, int THUMBNAIL_SIZE) throws IOException {


        InputStream input = context.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > THUMBNAIL_SIZE) ? (originalSize / THUMBNAIL_SIZE) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
        input = context.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    /**
     *
     *
     * @author Julian Frey
     *
     * @param ratio Round count
     * @return int
     */
    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    /**
     *
     * @author Julian Frey
     *
     * @param fullNote Note for FullNote
     */
    public void setFullNote(Notiz fullNote) {
        this.fullNote = fullNote;
    }

    /**
     *
     * @author Julian Frey
     *
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState instance state
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.createnotesfragment,
                container, false);
        super.onCreate(savedInstanceState);
        Button btnPicture = (Button) view.findViewById(R.id.btnPicture);
        btnPicture.setOnClickListener(this);
        Button btnShare = (Button) view.findViewById(R.id.btnShare);
        btnShare.setOnClickListener(this);
        Button btnSound = (Button) view.findViewById(R.id.btnSound);
        btnSound.setOnClickListener(this);
        btnStartSound = (Button) view.findViewById(R.id.btnTakeSound);
        btnStartSound.setOnClickListener(this);
        btnStopSound = (Button) view.findViewById(R.id.btnStopSound);
        btnStopSound.setOnClickListener(this);
        btnStopSound.setEnabled(false);
        Button btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        Button btnDelete = (Button) view.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(this);
        Button btnTakePicture = (Button) view.findViewById(R.id.btnTakePicture);
        btnTakePicture.setOnClickListener(this);
        Button btnTakeShare = (Button) view.findViewById(R.id.btnTakeShare);
        btnTakeShare.setOnClickListener(this);
        viewPicture = view.findViewById(R.id.layoutPicture);
        viewPicture.setVisibility(View.GONE);
        viewShare = view.findViewById(R.id.layoutShare);
        viewShare.setVisibility(View.GONE);
        viewSound = view.findViewById(R.id.layoutSound);
        viewSound.setVisibility(View.GONE);
        context = view.getContext();
        return view;
    }

    /**
     *
     * @author Julian Frey
     *
     * @param view the current view
     * @param savedInstanceState savedInstanceState
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        spinner = (Spinner) getView().findViewById(R.id.spinnerKat);
        ArrayAdapter<String> spinadapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, MainActivity.KategorieList);
        spinadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinadapter);

        txtdescription = (EditText) getView().findViewById(R.id.txtDescription);
        txtkeywords = (EditText) getView().findViewById(R.id.txtKeywords);
        txtplace = (EditText) getView().findViewById(R.id.txtPlace);
        txttitle = (EditText) getView().findViewById(R.id.txtTitle);
        if (!fullNote.getvNotizID().equals(null) && !fullNote.getvNotizID().isEmpty()) {
            txtdescription.setText(fullNote.getvDescription(), EditText.BufferType.EDITABLE);
            txtkeywords.setText(fullNote.getaKeyword(), EditText.BufferType.EDITABLE);
            txtplace.setText(fullNote.getvPlace(), EditText.BufferType.EDITABLE);
            txttitle.setText(fullNote.getvName(), EditText.BufferType.EDITABLE);
            fullNote.clearhPicture();
            fullNote.loadhPicture(fullNote.getvNotizID());
            for (int i = 0; i < MainActivity.KategorieList.size(); i++) {
                if (fullNote.getoKategorie().equals(MainActivity.KategorieList.get(i))) {
                    spinner.setSelection(i);

                }
            }

        }
        createImageLayout();
    }

    /**
     *
     * @author Julian Frey / Samuel Weidmann
     *
     * @param v clicked view
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnShare:
                if (viewShare.isShown()) {
                    viewShare.setVisibility(View.GONE);
                } else {
                    viewShare.setVisibility(View.VISIBLE);
                    drawShare();
                }
                break;
            case R.id.btnSound:
                if (viewSound.isShown()) {
                    viewSound.setVisibility(View.GONE);
                } else {
                    viewSound.setVisibility(View.VISIBLE);
                    drawSound();
                }
                break;
            case R.id.btnTakeSound:
                takeSound();
                break;
            case R.id.btnStopSound:
                stopSound();
                break;


            case R.id.btnPicture:

                if (viewPicture.isShown()) {
                    viewPicture.setVisibility(View.GONE);
                } else {
                    viewPicture.setVisibility(View.VISIBLE);
                    viewPicture.invalidate();
                    createImageLayout();
                }

                break;
            case R.id.btnTakePicture:
                dispatchTakePictureIntent();
                break;
            case R.id.btnTakeShare:
                Share share = new Share(context, this.fullNote);
                drawShare();

                break;
            case R.id.btnSave:
                fullNote.setvDescription(txtdescription.getText().toString());
                fullNote.setvPlace(txtplace.getText().toString());
                fullNote.setaKeyword(txtkeywords.getText().toString());
                fullNote.setvName(txttitle.getText().toString());
                fullNote.setoKategorie(spinner.getSelectedItem().toString());
                fullNote.saveNotiz();
                MainActivity.createNotiz();
                break;
            case R.id.btnDelete:
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Löschen?");
                alertDialog.setMessage("Wollen die die Notiz "+fullNote.getvName()+" wirklich löschen?");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ja", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        fullNote.deleteNote();
                        MainActivity.createNotiz();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, new NotesOverviewFragment(), "overview");
                        ft.commit();
                    }
                });
                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Nein", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
                break;
        }
        getView().findViewById(R.id.layoutNewNote).invalidate();


    }

    private void drawShare() {
        LinearLayout tmpLinLay = new LinearLayout(context);
        LinearLayout tmpShareLay = (LinearLayout) getActivity().findViewById(R.id.linShareDet);
        tmpLinLay.setOrientation(LinearLayout.VERTICAL);
        tmpShareLay.removeAllViews();
        for (int i = 0; i < fullNote.gethShare().size(); i++) {
            TextView share = new TextView(getActivity());
            share.setText((CharSequence) fullNote.gethShare().get(i));
            tmpLinLay.addView(share);

        }
        tmpShareLay.addView(tmpLinLay);
        tmpShareLay.invalidate();


    }

    /**
     *
     * @author Julian Frey
     *
     */
    private void drawSound() {
        final LinearLayout tmpSoundDisplay = (LinearLayout) getActivity().findViewById(R.id.linLayoutSoundDisplay);
        tmpSoundDisplay.removeAllViews();
        for (int i = 0; i < fullNote.gethSound().size(); i++) {

            Button tmpBtn = new Button(getActivity());

            tmpBtn.setText(new File(fullNote.gethSound().get(i)).getName());
            final int finalI = i;
            tmpBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final MediaPlayer[] mPlayer = {new MediaPlayer()};

                    LinearLayout tmpLinLay = new LinearLayout(context);
                    tmpLinLay.setOrientation(LinearLayout.VERTICAL);
                    final Button PlaySound = new Button(context);
                    final Button StopSound = new Button(context);
                    final Button PauseSound = new Button(context);

                    PlaySound.setText("Abspielen");
                    StopSound.setText("Stop");
                    PauseSound.setText("Pause");
                    StopSound.setEnabled(false);
                    PauseSound.setEnabled(false);

                    PlaySound.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                mPlayer[0] = new MediaPlayer();
                                mPlayer[0].setDataSource(fullNote.gethSound().get(finalI));
                                mPlayer[0].prepare();
                            } catch (IOException e) {
                                Log.e(LOG_TAG, "prepare() failed");
                            }
                            PlaySound.setEnabled(false);
                            StopSound.setEnabled(true);
                            PauseSound.setEnabled(true);
                            mPlayer[0].start();

                        }
                    });
                    PauseSound.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mPlayer[0].isPlaying()) {
                                mPlayer[0].pause();
                                PauseSound.setText("Resume");
                                PauseSound.invalidate();
                            } else {
                                mPlayer[0].start();
                                PauseSound.setText("Pause");
                                PauseSound.invalidate();
                            }

                        }
                    });
                    StopSound.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mPlayer[0].release();

                            PlaySound.setEnabled(true);
                            StopSound.setEnabled(false);
                            PauseSound.setEnabled(false);
                        }
                    });

                    tmpLinLay.addView(PlaySound);
                    tmpLinLay.addView(StopSound);
                    tmpLinLay.addView(PauseSound);
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Ton " + new File(fullNote.gethSound().get(finalI)).getName());
                    alertDialog.setView(tmpLinLay);
                    alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Delete", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mPlayer[0].release();
                            fullNote.removeSound(fullNote.gethSound().get(finalI));
                            drawSound();
                            tmpSoundDisplay.invalidate();


                        }
                    });

                    alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Back", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mPlayer[0].release();
                        }
                    });
                    alertDialog.show();

                }
            });

            tmpSoundDisplay.addView(tmpBtn);

        }
        tmpSoundDisplay.invalidate();


    }

    /**
     *
     * @author Julian Frey
     *
     */
    private void stopSound() {
        btnStopSound.setEnabled(false);
        btnStartSound.setEnabled(true);
        mediaRecorder.stop();
        mediaRecorder.release();
        viewSound.invalidate();
        drawSound();
    }

    /**
     *
     * @author Julian Frey
     */
    private void takeSound() {
        btnStartSound.setEnabled(false);
        btnStopSound.setEnabled(true);
        String mFileName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath().toString();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        mFileName += "/NoteAPPSound_" + timeStamp + ".3gp";

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(mFileName);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        fullNote.addSound(mFileName);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaRecorder.start();
        drawSound();
    }

    /**
     *
     * @author Julian Frey
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                System.out.println("Error in Image Creation");
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     *
     * @author Julian Frey
     *
     * @param requestCode requestCode
     * @param resultCode resultCode
     * @param data data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            viewPicture.invalidate();
            createImageLayout();
        }
    }

    /**
     *
     * @author Julian Frey
     *
     * @return File
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        fullNote.addPicture(mCurrentPhotoPath);
        return image;
    }

    /**
     *
     * @author Julian Frey / Samuel Weidmann
     */
    private void createImageLayout() {
        LinearLayout tmpRElLay = (LinearLayout) getActivity().findViewById(R.id.linImgLayout);
        tmpRElLay.removeAllViews();
        for (int i = 0; i < fullNote.gethPicture().size(); i++) {
            final ImageView tmpImgView = new ImageView(getActivity());
            tmpImgView.setId(i);
            final int id = i;
            View.OnClickListener clickListener = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ImageView tmpImgView1 = new ImageView(getActivity());
                    tmpImgView1.setId(id + 10);
                    try {
                        tmpImgView1.setImageBitmap(getThumbnail(Uri.parse(fullNote.gethPicture().get(id)), getView().getWidth()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Bild");
                    alertDialog.setView(tmpImgView1);
                    alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Delete", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            fullNote.removePicture(fullNote.gethPicture().get(id));
                            createImageLayout();
                            tmpImgView.invalidate();
                        }
                    });
                    alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Back", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            };
            tmpImgView.setOnClickListener(clickListener);
            try {
                tmpImgView.setImageBitmap(getThumbnail(Uri.parse(fullNote.gethPicture().get(i)), 200));
            } catch (IOException e) {
                e.printStackTrace();
            }
            tmpRElLay.addView(tmpImgView);
            tmpRElLay.invalidate();

            viewPicture.invalidate();
        }
    }
}

