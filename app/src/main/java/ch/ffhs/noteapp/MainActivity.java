package ch.ffhs.noteapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.ffhs.noteapp.R.layout.activity_main;


public class MainActivity extends Activity {
    public static Map<String, Notiz> hNotiz;
    public static Map<String, Kategorie> hKategorie;
    public static List KategorieList;
    public static NotesDatabase db;
    public static SQLiteDatabase database;
    private static Context appContext;
    private boolean consumedIntent = false;

    static {
        KategorieList = new ArrayList<String>();
        hKategorie = new HashMap<>();
        hNotiz = new HashMap<>();
    }

    /**
     * @author Julian Frey
     */
    private UserSettings userSettings;

    /**
     * @author Julian Frey
     */
    public MainActivity() {
    }

    /**
     * @return static
     * @author Julian Frey
     */
    public static Context getAppContext() {
        return appContext;
    }

    /**
     * @author Julian Frey
     */
    public static List createKategorie() {
        KategorieList.clear();
        String selectQuery = "SELECT name,Id FROM Category";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hKategorie.put(cursor.getString(0), new Kategorie(cursor.getString(0), cursor.getInt(1)));
            } while (cursor.moveToNext());
        }
        for (Map.Entry<String, Kategorie> KategorieValues : hKategorie.entrySet()) {
            KategorieList.add(KategorieValues.getKey());
        }
        Collections.sort(KategorieList);
        return KategorieList;
    }

    /**
     * @author Julian Frey
     */
    public static Map<String, Notiz> createNotiz() {
        hNotiz.clear();

        String selectQuery = "SELECT id FROM Note";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hNotiz.put(cursor.getString(0), new Notiz(cursor.getString(0), getAppContext()));
            } while (cursor.moveToNext());
        }
        return hNotiz;
    }

    /**
     * @author Julian Frey
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new NotesDatabase(this);
        database = db.getReadableDatabase();
        this.appContext = getApplicationContext();
        createKategorie();
        createNotiz();

        setContentView(activity_main);

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.fragment_container, new NotesOverviewFragment(), "overview").commit();
    }

    /**
     * @author Julian Frey
     */
    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        if (intent.getData() != null && consumedIntent == false) {
            consumedIntent = true;
            Uri uri = Uri.parse(intent.getData().toString());
            InputStream inputStream = null;
            try {

                XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = pullParserFactory.newPullParser();

                InputStream in_s = new FileInputStream(uri.getPath());
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in_s, null);
                parseXML(parser);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        Intent intentn = new Intent(this, MainActivity.class);
    }

    /**
     * @param xpp parser
     * @author Julian Frey
     */
    private void parseXML(XmlPullParser xpp) throws XmlPullParserException, IOException {
        int eventType = xpp.getEventType();
        Notiz currentNotiz = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_DOCUMENT) {
            } else if (eventType == XmlPullParser.START_TAG) {
                switch (xpp.getName()) {
                    case "Notiz":
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {
                            switch (xpp.getAttributeName(i)) {
                                case "Name":
                                    currentNotiz = new Notiz(xpp.getAttributeValue(i));
                                    break;
                                case "Ort":
                                    currentNotiz.setvPlace(xpp.getAttributeValue(i));
                                    break;
                                case "Datum":
                                    currentNotiz.setvDate(xpp.getAttributeValue(i));
                                    break;
                                case "Keywords":
                                    currentNotiz.setaKeyword(xpp.getAttributeValue(i));
                                    break;
                                case "Note":
                                    currentNotiz.setvDescription(xpp.getAttributeValue(i));
                                    break;
                                case "Category":
                                    if (gethKategorie().containsKey(xpp.getAttributeValue(i))) {
                                        currentNotiz.setoKategorie(xpp.getAttributeValue(i));
                                    } else {
                                        ContentValues values = new ContentValues();
                                        values.put("Name", xpp.getAttributeValue(i).toString());
                                        db.newCategory(database, values);
                                        int res = android.R.layout.simple_list_item_1;
                                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), res
                                                , MainActivity.KategorieList);
                                        adapter.notifyDataSetChanged();
                                    }

                                    break;


                            }
                        }
                        break;
                    case "Pictures":
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {

                            byte[] decodedString = Base64.decode(xpp.getAttributeValue(i), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            String imageFileName = "JPEG_" + timeStamp + "_";
                            File storageDir = Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_PICTURES);
                            File image = File.createTempFile(
                                    imageFileName,
                                    ".jpg",
                                    storageDir
                            );

                            String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
                            FileOutputStream fos = new FileOutputStream(image.getAbsolutePath());
                            decodedByte.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            fos.close();
                            currentNotiz.addPicture(mCurrentPhotoPath);

                        }
                        break;
                    case "Sounds":
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {

                            byte[] decodedString = Base64.decode(xpp.getAttributeValue(i), Base64.DEFAULT);
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            String soundFileName = "NoteAPPSound_" + timeStamp + "";
                            File storageDir = Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_MUSIC);
                            File sound = File.createTempFile(
                                    soundFileName,
                                    ".3gp",
                                    storageDir
                            );

                            String mFileName = "file:" + sound.getAbsolutePath();
                            FileOutputStream fos = new FileOutputStream(sound.getAbsolutePath());
                            fos.write(decodedString);
                            fos.close();
                            currentNotiz.addSound(mFileName);

                        }
                        break;
                }
            } else if (eventType == XmlPullParser.END_TAG) {
            } else if (eventType == XmlPullParser.TEXT) {
            }
            eventType = xpp.next();
        }
        currentNotiz.saveNotiz();
        HideFragments();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft = fm.beginTransaction();
        CreateNotesFragment frameNoteFragment = new CreateNotesFragment();
        frameNoteFragment.setFullNote(new Notiz(currentNotiz.getvNotizID(), getApplicationContext()));
        ft.replace(R.id.fragment_container, frameNoteFragment, "overview");
        ft.commit();

    }

    /**
     * @author Julian Frey
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * @param menu
     * @author Julian Frey
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * @author Julian Frey
     */
    public Notiz newNotiz() {
        Notiz notiz = new Notiz("");
        return notiz;
    }

    public void removeNotiz() {
    }

    /**
     * @param key string
     * @author Julian Frey
     */
    public Notiz getNotiz(String key) {
        return hNotiz.get(key);
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    public Map<String, Notiz> getNotiz() {
        return hNotiz;
    }

    public void sethNotiz(HashMap<String, Notiz> hNotiz) {
        this.hNotiz = hNotiz;
    }

    public Map<String, Kategorie> gethKategorie() {
        return hKategorie;
    }

    public void sethKategorie(Map<Integer, Kategorie> hKategorie) {
    }

    /**
     * @param searchString Search String
     * @param keyword      keywords
     * @param date         date
     * @param ort          place
     * @param plz          plz
     * @return Map
     * @author Julian Frey
     */
    public Map<String, Notiz> searchNotizList(String searchString, String keyword, Date date, String ort, String plz) {
        return hNotiz;
    }

    /**
     * @param item selected Item
     * @author Julian Frey
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch (item.getItemId()) {
            case R.id.action_notes:
                HideFragments();
                ft.replace(R.id.fragment_container, new NotesOverviewFragment(), "overview");
                ft.commit();
                return true;
            case R.id.action_newNote:
                HideFragments();
                ft = fm.beginTransaction();
                CreateNotesFragment frameNoteFragment = new CreateNotesFragment();
                frameNoteFragment.setFullNote(new Notiz("TestNote"));
                ft.replace(R.id.fragment_container, frameNoteFragment, "overview");
                ft.commit();
                return true;
            case R.id.action_settings:
                HideFragments();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment_container, new SettingsFragment(), "overview");
                ft.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * @author Julian Frey
     */
    public void HideFragments() {
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().remove(fm.findFragmentByTag("overview")).commit();

    }

    /**
     * @author Julian Frey
     */
    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        HideFragments();

        ft.replace(R.id.fragment_container, new NotesOverviewFragment(), "overview");
        ft.commit();
    }
}
