package ch.ffhs.noteapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Julian Frey on 03.12.2014.
 * Date: 03.12.2014
 */
public class UserSettings {
    ContentValues values;
    private String vPersID;
    private String vPrename;
    private String vSurename;
    private String vPLZ;
    private String vPlace;
    private String vStreet;

    /**
     * Auslesen der Einstellungen aus der Datenbank
     *
     * @param db
     * @author Samuel Weimdannn
     */
    public UserSettings(SQLiteDatabase db) {
        String selectQuery = "SELECT * FROM PersonalData";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            vPersID = cursor.getString(0);
            vPrename = cursor.getString(2);
            vSurename = cursor.getString(1);
            vPLZ = cursor.getString(5);
            vPlace = cursor.getString(4);
            vStreet = cursor.getString(3);
        }
    }

    /**
     * Ändern der Einstellungen in der Datenbank
     *
     * @param db
     * @param database
     * @param vPersID
     * @param vPrename
     * @param vSurename
     * @param vPLZ
     * @param vPlace
     * @param vStreet
     * @author Samuel Weidmann
     */
    public void setUser(NotesDatabase db, SQLiteDatabase database, String vPersID, String vPrename, String vSurename, String vPLZ, String vPlace, String vStreet) {

        this.vPersID = vPersID;
        this.vPrename = vPrename;
        this.vSurename = vSurename;
        this.vPLZ = vPLZ;
        this.vPlace = vPlace;
        this.vStreet = vStreet;
        values = new ContentValues();
        values.put("Surname", vSurename);
        values.put("Firstname", vPrename);
        values.put("Street", vStreet);
        values.put("City", vPlace);
        values.put("Plz", vPLZ);

    }

    /**
     * Getter für den Vorname
     *
     * @return Vorname in den Einstellungen
     * @author Julian Frey
     */
    public String getvPrename() {
        return vPrename;
    }

    /**
     * Setter des Nachnamen
     *
     * @param vPrename
     * @author Julian Frey
     */
    public void setvPrename(String vPrename) {
        this.vPrename = vPrename;
    }

    /**
     * Setter für den Vorname
     *
     * @param vPrename
     * @author Julian Frey
     */
    public void setPrename(String vPrename) {
        this.vPrename = vPrename;
    }

    /**
     * Getter für die ID des Users
     *
     * @return ID des Users aus den Einstellungen
     * @author Julian Frey
     */
    public String getvPersID() {
        return vPersID;
    }

    /**
     * Setter der ID des Users
     *
     * @param vPersID
     * @author Julian Frey
     */
    public void setvPersID(String vPersID) {
        this.vPrename = vPersID;
    }

    /**
     * Getter des Orts
     *
     * @return Ort aus den Einstellungen
     * @author Julian Frey
     */
    public String getvPlace() {
        return vPlace;
    }

    /**
     * Setter des Orts
     *
     * @param vPlace
     * @author Julian Frey
     */
    public void setvPlace(String vPlace) {
        this.vPlace = vPlace;
    }

    /**
     * Getter des Nachnamen
     *
     * @return Nachname aus den Einstellungen
     * @author Julian Frey
     */
    public String getvSurename() {
        return vSurename;
    }

    /**
     * Setter für den Nachname
     *
     * @param vSurename
     * @author Julian Frey
     */
    public void setvSurename(String vSurename) {
        this.vSurename = vSurename;
    }

    /**
     * Getter für alle Werte
     *
     * @return Werte der Einstellungen
     * @author Julian Frey
     */
    public ContentValues getValues() {
        return values;
    }

    /**
     * Setter für alle Werte
     *
     * @param values
     * @author Julian Frey
     */
    public void setValues(ContentValues values) {
        this.values = values;
    }

    /**
     * Getter für die PLZ
     *
     * @return PLZ der Einstellungen
     * @author Julian Frey
     */
    public String getvPLZ() {
        return vPLZ;
    }

    /**
     * Setter für die PLZ
     *
     * @param vPLZ
     * @author Julian Frey
     */
    public void setvPLZ(String vPLZ) {
        this.vPLZ = vPLZ;
    }

    /**
     * Getter für Strasse
     *
     * @return Strasse in den Einstellungen
     * @author Julian Frey
     */
    public String getvStreet() {
        return vStreet;
    }

    /**
     * Setter für die Strasse
     *
     * @param vStreet
     * @author Julian Frey
     */
    public void setvStreet(String vStreet) {
        this.vStreet = vStreet;
    }
}
