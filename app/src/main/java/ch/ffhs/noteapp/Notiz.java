package ch.ffhs.noteapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by julian on 03.12.2014.
 * Date: 03.12.2014
 */
public class Notiz {
    public static NotesDatabase db;
    public static SQLiteDatabase database;
    private ArrayList<String> hSound = new ArrayList<>();
    private ArrayList<String> hPicture = new ArrayList<>();
    private ArrayList<String> hShare = new ArrayList<>();
    private String vName = "";
    private String aKeyword = "";
    private String vKategorie = "";
    private String vPlace = "";
    private String vDescription = "";
    private String vNotizID;
    private String vDate;

    /**
     * @param vNotizID Notiz ID
     * @param context  context
     * @author Julian Frey
     */
    public Notiz(String vNotizID, Context context) {
        this.vNotizID = vNotizID;
        db = new NotesDatabase(context);
        database = db.getReadableDatabase();
        String selectQuery = "SELECT n.Title, n.Keywords, n.Description, n.City, n.Date, c.Name FROM Note n, Category c where n.ID =" + vNotizID + " and n.Category_Id=c.ID";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            vName = cursor.getString(0);
            aKeyword = cursor.getString(1);
            vDescription = cursor.getString(2);
            vPlace = cursor.getString(3);
            vDate = cursor.getString(4);
            vKategorie = cursor.getString(5);
        }
        loadhPicture();
        loadhSound();
        loadShare(vNotizID);
    }

    /**
     * @param vName Name der Notiz
     * @author Julian Frey
     * Wird verwendet um eine neue Notiz zu erstellen
     */
    public Notiz(String vName) {
        this.vName = vName;
        this.vNotizID = "";
    }

    /**
     * @author Julian Frey
     */
    public String getvDate() {
        return vDate;
    }

    /**
     * @param vDate Date
     * @author Julian Frey
     */
    public void setvDate(String vDate) {
        this.vDate = vDate;
    }

    /**
     * @author Julian Frey
     */
    public String getvNotizID() {
        return vNotizID;
    }

    /**
     * @param vNotizID Notiz ID
     * @author Julian Frey
     */
    public void setvNotizID(String vNotizID) {
        this.vNotizID = vNotizID;
    }

    /**
     * @param vNotizID Notiz ID
     * @return ArrayList
     * @author Julian Frey
     */
    public ArrayList<String> loadShare(String vNotizID) {
        String selectQuery = "SELECT * from Share where Note_ID=" + vNotizID;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hShare.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return hShare;
    }

    /**
     * @return ArrayList
     * @author Julian Frey
     */
    public ArrayList<String> loadhPicture() {
        String selectQuery = "SELECT Picture from Picture where Note_ID=" + vNotizID;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hPicture.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return hPicture;
    }

    /**
     * @param vNotizID Notiz ID
     * @return ArrayList
     * @author Julian Frey
     */
    public ArrayList<String> loadhPicture(String vNotizID) {
        String selectQuery = "SELECT Picture from Picture where Note_ID=" + vNotizID;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hPicture.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return hPicture;
    }

    /**
     * @return ArrayList
     * @author Julian Frey
     */
    public ArrayList<String> loadhSound() {
        String selectQuery = "SELECT * from Sound where Note_ID=" + this.getvNotizID();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hSound.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return hSound;
    }

    /**
     * @return ArrayList
     * @author Julian Frey
     */
    public ArrayList<String> gethShare() {
        return hShare;
    }

    public void sethShare(ArrayList<String> hShare) {
        this.hShare = hShare;
    }

    public ArrayList<String> gethPicture() {
        return hPicture;
    }

    public void sethPicture(ArrayList<String> hPicture) {
        this.hPicture = hPicture;
    }

    public void addPicture(String picture) {
        this.hPicture.add(picture);
    }

    public void addSound(String sound) {
        this.hSound.add(sound);
    }

    public void addShare(String share) {
        this.hShare.add(share);
    }

    /**
     * @param picture zu entfernendes Bild
     * @author Julian Frey
     */
    public void removePicture(String picture) {
        this.hPicture.remove(picture);
        database.delete("Picture", "Picture='" + picture + "'", null);
        try {
            File file = new File(picture);
            if (file.exists()) {

                boolean result = file.delete();
                System.out.println("Application able to delete the file and result is: " + result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param sound zu entfernendes Sound
     * @author Julian Frey
     */
    public void removeSound(String sound) {
        this.hSound.remove(sound);
        database.delete("Sound", "Sound='" + sound + "'", null);
        try {
            File file = new File(sound);
            if (file.exists()) {

                boolean result = file.delete();
                System.out.println("Application able to delete the file and result is: " + result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getvName() {
        return vName;
    }

    public void setvName(String vName) {
        this.vName = vName;
    }

    public String getaKeyword() {
        return aKeyword;
    }

    public void setaKeyword(String aKeyword) {
        this.aKeyword = aKeyword;
    }

    public String getoKategorie() {
        return vKategorie;
    }

    public void setoKategorie(String vKategorie) {
        this.vKategorie = vKategorie;
    }

    public String getvPlace() {
        return vPlace;
    }

    public void setvPlace(String vPlace) {
        this.vPlace = vPlace;
    }

    public String getvDescription() {
        return vDescription;
    }

    public void setvDescription(String vDescription) {
        this.vDescription = vDescription;
    }

    public ArrayList<String> gethSound() {
        return hSound;
    }

    public void sethSound(ArrayList<String> hSound) {
        this.hSound = hSound;
    }

    /**
     * @author Julian Frey
     * Speichert die Notiz
     */
    public void saveNotiz() {
        ContentValues values = new ContentValues();

        if (getvNotizID().isEmpty()) {
            values.put("Title", getvName());
            values.put("Keywords", getaKeyword());
            values.put("Description", getvDescription());
            values.put("City", getvPlace());
            values.put("Category_Id", MainActivity.hKategorie.get(getoKategorie()).getvID());


            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());


            values.put("Date", date);
            long id = database.insert("Note", null, values);
            setvNotizID(Long.toString(id));
            database.delete("Picture", "Note_Id =" + id, null);
            database.delete("Sound", "Note_Id =" + id, null);
            database.delete("Share", "Note_Id =" + id, null);
            for (int i = 0; i < gethPicture().size(); i++) {
                values = new ContentValues();
                values.put("Note_Id", id);
                values.put("Picture", gethPicture().get(i));
                database.insert("Picture", null, values);
            }
            for (int i = 0; i < gethSound().size(); i++) {
                values = new ContentValues();
                values.put("Note_Id", id);
                values.put("Sound", gethSound().get(i));
                database.insert("Sound", null, values);
            }
            for (int i = 0; i < gethShare().size(); i++) {
                values = new ContentValues();
                values.put("Note_Id", id);
                values.put("Email", gethSound().get(i));
                database.insert("Share", null, values);
            }
        } else {
            values.put("Title", getvName().toString());
            values.put("Keywords", getaKeyword().toString());
            values.put("Description", getvDescription());
            values.put("City", getvPlace());

            values.put("Category_Id", MainActivity.hKategorie.get(getoKategorie()).getvID());

            values.put("Date", "2014.12.12");
            database.update("Note", values, "Id =" + getvNotizID(), null);
            database.delete("Picture", "Note_Id =" + getvNotizID(), null);
            database.delete("Sound", "Note_Id =" + getvNotizID(), null);
            database.delete("Share", "Note_Id =" + getvNotizID(), null);
            for (int i = 0; i < gethPicture().size(); i++) {
                values = new ContentValues();
                values.put("Note_Id", getvNotizID());
                values.put("Picture", gethPicture().get(i));
                database.insert("Picture", null, values);
            }
            for (int i = 0; i < gethSound().size(); i++) {
                values = new ContentValues();
                values.put("Note_Id", getvNotizID());
                values.put("Sound", gethSound().get(i));
                database.insert("Sound", null, values);
            }
            for (int i = 0; i < gethShare().size(); i++) {
                values = new ContentValues();
                values.put("Note_Id", getvNotizID());
                values.put("Email", gethShare().get(i));
                database.insert("Share", null, values);
            }
        }
    }

    /**
     * @author Julian Frey
     * Notiz Loeschen
     */
    public void deleteNote() {
        for (int i = 0; i < gethPicture().size(); i++) {
            removePicture(gethPicture().get(i));
        }
        for (int i = 0; i < gethSound().size(); i++) {
            removeSound(gethSound().get(i));
        }
        database.delete("Share", "Note_Id=" + getvNotizID(), null);
        database.delete("Note", "ID=" + getvNotizID(), null);
        MainActivity.hNotiz.remove(getvNotizID());
    }

    public void clearhPicture() {
        this.hPicture = new ArrayList<>();
    }
}
