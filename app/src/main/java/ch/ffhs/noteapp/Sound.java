package ch.ffhs.noteapp;

/**
 * Created by Julian Frey on 03.12.2014.
 * Date: 03.12.2014
 */
public class Sound {
    private String vID;
    //private Sound vSound;
    private String vSound;

    /**
     * @param vID
     * @author Julian Frey
     */
    public Sound(String vID) {
        this.vID = vID;
    }

    /**
     * Getter für die Ton ID
     *
     * @return ID des Tons
     * @author Julian Frey
     */
    public String getvID() {
        return vID;
    }

    /**
     * Getter für den Ton
     *
     * @return Ton
     * @author Julian Frey
     */
    public String getvSound() {
        return vSound;
    }

    /**
     * Setter für den Ton
     *
     * @param vSound
     * @author Julian Frey
     */
    public void setvSound(String vSound) {
        this.vSound = vSound;
    }

}
